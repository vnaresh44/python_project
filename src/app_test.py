import unittest
from app import fibonacci,factorial
import pandas
class TestFibonacci(unittest.TestCase):
    def test_fibonacci_1(self):
        self.assertEqual(fibonacci(1),1)

    def test_fibonacci_10(self):
        self.assertEqual(fibonacci(10),55)
    
    def test_fibonacci_30(self):
        self.assertEqual(fibonacci(30),832040)

class TestFactorial(unittest.TestCase):
    def test_factorial_1(self):
        self.assertEqual(factorial(1),1)

    def test_factorial_5(self):
        self.assertEqual(factorial(5),120)
    
    def test_factorial_10(self):
        self.assertEqual(factorial(10),3628800)

